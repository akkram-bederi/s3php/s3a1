<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<div style="border: 1px solid black; padding:20px; margin:auto; width:500px; margin-top: 15%;">
		<h1>Person</h1>
		<p><?= $person->printName() ?></p>

		<h1>Developer</h1>
		<p><?= $developer->printName() ?></p>

		<h1>Engineer</h1>
		<p><?= $engineer->printName();?></p>

	</div>
</body>
</html>