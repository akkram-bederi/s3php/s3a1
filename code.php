<?php


class Person {

	public $firstName;
	public $lastName;

	function __construct($firstName,$lastName){
		$this->firstName=$firstName;
		$this->lastName=$lastName;
		
	}

	public function printName(){
		return "Your full name is $this->firstName $this->lastName";
	}
}

class Developer extends Person{
	public function printName(){
		return "Your name is $this->firstName $this->lastName and you are a developer";
	}
}

class Engineer extends Person{
	public function printName(){
		return "You are an engineer named $this->firstName $this->lastName"; 
	}
}


$person=new Person("Senku","Ishigami");

$developer= new Developer("John Finch","Smith");

$engineer= new Engineer("Harold Myers","Reese");



